package com.leonia.git;

public class Ladder {
	
		
		private static Integer ladder1startpoint = 2;
		private static Integer ladder1endpoint = 38;
		private static Integer ladder2startpoint = 7;
		private static Integer ladder2endpoint = 14;
		private static Integer ladder3startpoint = 8;
		private static Integer ladder3endpoint = 31;
		private static Integer ladder4startpoint = 15;
		private static Integer ladder4endpoint = 26;
		private static Integer ladder5startpoint = 21;
		private static Integer ladder5endpoint = 42;
		private static Integer ladder6startpoint = 28;
		private static Integer ladder6endpoint = 84;
		private static Integer ladder7startpoint = 36;
		private static Integer ladder7endpoint = 44;
		private static Integer ladder8startpoint = 51;
		private static Integer ladder8endpoint = 67;
		private static Integer ladder9startpoint = 78;
		private static Integer ladder9endpoint = 98;
		private static Integer ladder10startpoint = 71;
		private static Integer ladder10endpoint = 91;
		private static Integer ladder11startpoint = 87;
		private static Integer ladder11endpoint = 94;
		
		public static Integer getLadder1startpoint() {
			return ladder1startpoint;
		}
		public static void setLadder1startpoint(Integer ladder1startpoint) {
			Ladder.ladder1startpoint = ladder1startpoint;
		}
		public static Integer getLadder1endpoint() {
			return ladder1endpoint;
		}
		public static void setLadder1endpoint(Integer ladder1endpoint) {
			Ladder.ladder1endpoint = ladder1endpoint;
		}
		public static Integer getLadder2startpoint() {
			return ladder2startpoint;
		}
		public static void setLadder2startpoint(Integer ladder2startpoint) {
			Ladder.ladder2startpoint = ladder2startpoint;
		}
		public static Integer getLadder2endpoint() {
			return ladder2endpoint;
		}
		public static void setLadder2endpoint(Integer ladder2endpoint) {
			Ladder.ladder2endpoint = ladder2endpoint;
		}
		public static Integer getLadder3startpoint() {
			return ladder3startpoint;
		}
		public static void setLadder3startpoint(Integer ladder3startpoint) {
			Ladder.ladder3startpoint = ladder3startpoint;
		}
		public static Integer getLadder3endpoint() {
			return ladder3endpoint;
		}
		public static void setLadder3endpoint(Integer ladder3endpoint) {
			Ladder.ladder3endpoint = ladder3endpoint;
		}
		public static Integer getLadder4startpoint() {
			return ladder4startpoint;
		}
		public static void setLadder4startpoint(Integer ladder4startpoint) {
			Ladder.ladder4startpoint = ladder4startpoint;
		}
		public static Integer getLadder4endpoint() {
			return ladder4endpoint;
		}
		public static void setLadder4endpoint(Integer ladder4endpoint) {
			Ladder.ladder4endpoint = ladder4endpoint;
		}
		public static Integer getLadder5startpoint() {
			return ladder5startpoint;
		}
		public static void setLadder5startpoint(Integer ladder5startpoint) {
			Ladder.ladder5startpoint = ladder5startpoint;
		}
		public static Integer getLadder5endpoint() {
			return ladder5endpoint;
		}
		public static void setLadder5endpoint(Integer ladder5endpoint) {
			Ladder.ladder5endpoint = ladder5endpoint;
		}
		public static Integer getLadder6startpoint() {
			return ladder6startpoint;
		}
		public static void setLadder6startpoint(Integer ladder6startpoint) {
			Ladder.ladder6startpoint = ladder6startpoint;
		}
		public static Integer getLadder6endpoint() {
			return ladder6endpoint;
		}
		public static void setLadder6endpoint(Integer ladder6endpoint) {
			Ladder.ladder6endpoint = ladder6endpoint;
		}
		public static Integer getLadder7startpoint() {
			return ladder7startpoint;
		}
		public static void setLadder7startpoint(Integer ladder7startpoint) {
			Ladder.ladder7startpoint = ladder7startpoint;
		}
		public static Integer getLadder7endpoint() {
			return ladder7endpoint;
		}
		public static void setLadder7endpoint(Integer ladder7endpoint) {
			Ladder.ladder7endpoint = ladder7endpoint;
		}
		public static Integer getLadder8startpoint() {
			return ladder8startpoint;
		}
		public static void setLadder8startpoint(Integer ladder8startpoint) {
			Ladder.ladder8startpoint = ladder8startpoint;
		}
		public static Integer getLadder8endpoint() {
			return ladder8endpoint;
		}
		public static void setLadder8endpoint(Integer ladder8endpoint) {
			Ladder.ladder8endpoint = ladder8endpoint;
		}
		public static Integer getLadder9startpoint() {
			return ladder9startpoint;
		}
		public static void setLadder9startpoint(Integer ladder9startpoint) {
			Ladder.ladder9startpoint = ladder9startpoint;
		}
		public static Integer getLadder9endpoint() {
			return ladder9endpoint;
		}
		public static void setLadder9endpoint(Integer ladder9endpoint) {
			Ladder.ladder9endpoint = ladder9endpoint;
		}
		public static Integer getLadder10startpoint() {
			return ladder10startpoint;
		}
		public static void setLadder10startpoint(Integer ladder10startpoint) {
			Ladder.ladder10startpoint = ladder10startpoint;
		}
		public static Integer getLadder10endpoint() {
			return ladder10endpoint;
		}
		public static void setLadder10endpoint(Integer ladder10endpoint) {
			Ladder.ladder10endpoint = ladder10endpoint;
		}
		public static Integer getLadder11startpoint() {
			return ladder11startpoint;
		}
		public static void setLadder11startpoint(Integer ladder11startpoint) {
			Ladder.ladder11startpoint = ladder11startpoint;
		}
		public static Integer getLadder11endpoint() {
			return ladder11endpoint;
		}
		public static void setLadder11endpoint(Integer ladder11endpoint) {
			Ladder.ladder11endpoint = ladder11endpoint;
		}
		
	}

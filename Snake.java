package com.leonia.git;

public class Snake {
	

		private static Integer snake1startpoint = 16;
		private static Integer snake1endpoint = 6;
		private static Integer snake2startpoint = 46;
		private static Integer snake2endpoint = 25;
		private static Integer snake3startpoint = 49;
		private static Integer snake3endpoint = 11;
		private static Integer snake4startpoint = 62;
		private static Integer snake4endpoint = 19;
		private static Integer snake5startpoint = 64;
		private static Integer snake5endpoint = 60;
		private static Integer snake6startpoint = 74;
		private static Integer snake6endpoint = 53;
		private static Integer snake7startpoint = 89;
		private static Integer snake7endpoint = 68;
		private static Integer snake8startpoint = 92;
		private static Integer snake8endpoint = 88;
		private static Integer snake9startpoint = 95;
		private static Integer snake9endpoint = 75;
		private static Integer snake10startpoint = 99;
		private static Integer snake10endpoint = 80;
		
		public static Integer getSnake1startpoint() {
			return snake1startpoint;
		}
		public static void setSnake1startpoint(Integer snake1startpoint) {
			Snake.snake1startpoint = snake1startpoint;
		}
		public static Integer getSnake1endpoint() {
			return snake1endpoint;
		}
		public static void setSnake1endpoint(Integer snake1endpoint) {
			Snake.snake1endpoint = snake1endpoint;
		}
		public static Integer getSnake2startpoint() {
			return snake2startpoint;
		}
		public static void setSnake2startpoint(Integer snake2startpoint) {
			Snake.snake2startpoint = snake2startpoint;
		}
		public static Integer getSnake2endpoint() {
			return snake2endpoint;
		}
		public static void setSnake2endpoint(Integer snake2endpoint) {
			Snake.snake2endpoint = snake2endpoint;
		}
		public static Integer getSnake3startpoint() {
			return snake3startpoint;
		}
		public static void setSnake3startpoint(Integer snake3startpoint) {
			Snake.snake3startpoint = snake3startpoint;
		}
		public static Integer getSnake3endpoint() {
			return snake3endpoint;
		}
		public static void setSnake3endpoint(Integer snake3endpoint) {
			Snake.snake3endpoint = snake3endpoint;
		}
		public static Integer getSnake4startpoint() {
			return snake4startpoint;
		}
		public static void setSnake4startpoint(Integer snake4startpoint) {
			Snake.snake4startpoint = snake4startpoint;
		}
		public static Integer getSnake4endpoint() {
			return snake4endpoint;
		}
		public static void setSnake4endpoint(Integer snake4endpoint) {
			Snake.snake4endpoint = snake4endpoint;
		}
		public static Integer getSnake5startpoint() {
			return snake5startpoint;
		}
		public static void setSnake5startpoint(Integer snake5startpoint) {
			Snake.snake5startpoint = snake5startpoint;
		}
		public static Integer getSnake5endpoint() {
			return snake5endpoint;
		}
		public static void setSnake5endpoint(Integer snake5endpoint) {
			Snake.snake5endpoint = snake5endpoint;
		}
		public static Integer getSnake6startpoint() {
			return snake6startpoint;
		}
		public static void setSnake6startpoint(Integer snake6startpoint) {
			Snake.snake6startpoint = snake6startpoint;
		}
		public static Integer getSnake6endpoint() {
			return snake6endpoint;
		}
		public static void setSnake6endpoint(Integer snake6endpoint) {
			Snake.snake6endpoint = snake6endpoint;
		}
		public static Integer getSnake7startpoint() {
			return snake7startpoint;
		}
		public static void setSnake7startpoint(Integer snake7startpoint) {
			Snake.snake7startpoint = snake7startpoint;
		}
		public static Integer getSnake7endpoint() {
			return snake7endpoint;
		}
		public static void setSnake7endpoint(Integer snake7endpoint) {
			Snake.snake7endpoint = snake7endpoint;
		}
		public static Integer getSnake8startpoint() {
			return snake8startpoint;
		}
		public static void setSnake8startpoint(Integer snake8startpoint) {
			Snake.snake8startpoint = snake8startpoint;
		}
		public static Integer getSnake8endpoint() {
			return snake8endpoint;
		}
		public static void setSnake8endpoint(Integer snake8endpoint) {
			Snake.snake8endpoint = snake8endpoint;
		}
		public static Integer getSnake9startpoint() {
			return snake9startpoint;
		}
		public static void setSnake9startpoint(Integer snake9startpoint) {
			Snake.snake9startpoint = snake9startpoint;
		}
		public static Integer getSnake9endpoint() {
			return snake9endpoint;
		}
		public static void setSnake9endpoint(Integer snake9endpoint) {
			Snake.snake9endpoint = snake9endpoint;
		}
		public static Integer getSnake10startpoint() {
			return snake10startpoint;
		}
		public static void setSnake10startpoint(Integer snake10startpoint) {
			Snake.snake10startpoint = snake10startpoint;
		}
		public static Integer getSnake10endpoint() {
			return snake10endpoint;
		}
		public static void setSnake10endpoint(Integer snake10endpoint) {
			Snake.snake10endpoint = snake10endpoint;
		}
		
	}


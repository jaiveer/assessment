# Debugging Code

Take a look at the class in WordAnalyzer.java.

## 1. firstRepeatedCharacter

### 1.1 Look at the function **firstRepeatedCharacter**. Without running the code, write down the expected output of this function in the following cases
assuming the function has been implemented correctly.

1.1.1 
```
WordAnalyzer wa = new WordAnalyzer("aardvark");
char result = wa.firstRepeatedCharacter();
if (result == 0)
         System.out.println("No repeated character.");
      else
         System.out.println("First repeated character = " + result);
```

1.1.2. 
```
WordAnalyzer wa = new WordAnalyzer("roommate");
char result = wa.firstRepeatedCharacter();
if (result == 0)
         System.out.println("No repeated character.");
      else
         System.out.println("First repeated character = " + result);
```

1.1.3. 
```
WordAnalyzer wa = new WordAnalyzer("mate");
char result = wa.firstRepeatedCharacter();
if (result == 0)
         System.out.println("No repeated character.");
      else
         System.out.println("First repeated character = " + result);
```

1.1.4. 
```
WordAnalyzer wa = new WordAnalyzer("test");
char result = wa.firstRepeatedCharacter();
if (result == 0)
         System.out.println("No repeated character.");
      else
         System.out.println("First repeated character = " + result);
```

### 1.2 Now run the code. What is the actual output of each of the above scenarios? Write down the output below the corresponding word.
If there are errors, write that there are errors.

1.2.1. "aardvark"

1.2.2 "roommate"

1.2.3 "mate"

1.2.4 "test"

### 1.3 Are there any bugs in the code for `firstRepeatedCharacter`? If yes, explain what the bug is and fix the code. For this question, write the corrected
version of `firstRepeatedCharacter`. Copy the function as is if you believe there are no bugs.

### 1.4 What would happen if the word were null? Please suggest what to do to handle the null case. 


## 2. firstMultipleCharacter

### 2.1 Look at the function **firstMultipleCharacter**. Without running the code, write down the expected output of this function in the following cases
assuming the function has been implemented correctly.

2.1.1
```
      WordAnalyzer wa = new WordAnalyzer("missisippi");
      char result = wa.firstMultipleCharacter();
      if (result == 0)
         System.out.println("No multiple character.");
      else
         System.out.println("First multiple character = " + result);
```

2.1.2
```
      WordAnalyzer wa = new WordAnalyzer("mate");
      char result = wa.firstMultipleCharacter();
      if (result == 0)
         System.out.println("No multiple character.");
      else
         System.out.println("First multiple character = " + result);
```

2.1.3
```
      WordAnalyzer wa = new WordAnalyzer("test");
      char result = wa.firstMultipleCharacter();
      if (result == 0)
         System.out.println("No multiple character.");
      else
         System.out.println("First multiple character = " + result);
```

### 2.2 Now run the code. What is the actual output of each of the above scenarios? Write down the output below the corresponding word.
If there are errors, write that there are errors.

2.2.1. "missisippi"

2.2.2 "mate"

2.2.3 "test"

### 2.3 Are there any bugs in the code for `firstMultipleCharacter`? If yes, explain what the bug is and fix the code. For this question, write the corrected
version of `firstMultipleCharacter`. Copy the function as is if you believe there are no bugs.

## 3. countRepeatedCharacters

### 3.1 Look at the function **countRepeatedCharacters**. Without running the code, write down the expected output of this function in the following cases
assuming the function has been implemented correctly.

3.1.1
```
      WordAnalyzer wa = new WordAnalyzer("missisippi");
      int result = wa.countRepeatedCharacters();
      System.out.println(result + " repeated characters.");
```

3.1.2
```
      WordAnalyzer wa = new WordAnalyzer("test");
      int result = wa.countRepeatedCharacters();
      System.out.println(result + " repeated characters.");
```

3.1.3
```
      WordAnalyzer wa = new WordAnalyzer("aabbcdaaaabb");
      int result = wa.countRepeatedCharacters();
      System.out.println(result + " repeated characters.");
```

### 3.2 Now run the code. What is the actual output of each of the above scenarios? Write down the output below the corresponding word.
If there are errors, write that there are errors.

3.2.1. "missisippi"

3.2.2 "test"

3.2.3 "aabbcdaaaabb"

### 3.3 Are there any bugs in the code for `countRepeatedCharacters`? If yes, explain what the bug is and fix the code. For this question, write the corrected
version of `countRepeatedCharacters`. Copy the function as is if you believe there are no bugs.

### 4. What did you do to help you in the debugging process? System.out.println statements? Breakpoints?

### 5. Are you familiar with design patterns? Please list your 3 favorite design patterns and when you think they should be used.

